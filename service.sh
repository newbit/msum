#!/system/bin/sh
MODDIR=${0%/*}
settings put global guest_user_enabled 0

local boot_completed=0
while [ $boot_completed != "1" ]; do
	sleep 0.5
	boot_completed=$(getprop dev.bootcomplete)
done

local userID=""
local userZero=0
userID=$(am get-current-user)
if [ "$userID" != "$userZero" ]; then
	am switch-user $userZero
	userID=$(am get-current-user)
fi	
userIDs=$(pm list users)
for userID in $userIDs; do
	if [[ "$userID" == *"UserInfo"* ]]; then
		userID=${userID##*UserInfo{}
		userID=${userID%%:*}
		if [ "$userID" != "$userZero" ]; then
			pm remove-user $userID
		fi		
	fi		
done