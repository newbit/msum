# Magisk Single User Mod
### [NewBit @ xda-developers](https://forum.xda-developers.com/m/newbit.1350876)

### Description
Disables the multiple users feature, including the guest account.
The module waits for the device boot complete state, in a none blocking
stage, and then removes all users higher than 0.

Originally from [seebz](https://github.com/seebz/magisk-single-user)
ported/updated from [NewBit @ xda-developers](https://forum.xda-developers.com/m/newbit.1350876)
